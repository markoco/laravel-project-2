<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'QuizController@index')->name('home');

Route::get('/unauthorized', function () {
    return view('404');
});
Route::delete('/deletequiz/{id}', 'QuizController@deletequiz');


Route::middleware("user")->group(function(){


	Route::get('/landingpage', 'QuizController@index');

	Route::post('/createquiz', 'QuizController@createquiz');

	Route::post('/submitanswer', 'AnswerController@submitanswer');

	Route::get('/myquizzes', 'QuizController@myquizzes');

	Route::get('/updatequiz/{id}', 'QuizController@updatequiz');

	Route::patch('/updatequiz/{id}', 'QuizController@editquiz');

	Route::get('/reviewquiz', 'AnswerController@reviewansweredquiz');

	Route::get('/quiz/{id}', 'QuizController@filter');

	Route::get('/quizprofile/{id}', 'QuizController@filterprofile');

	Route::get('/quizreview/{id}', 'AnswerController@filterreview');

	Route::post('/search', 'QuizController@search');

	Route::post('/searchprofile', 'QuizController@searchprofile');

	Route::post('/like/{id}', 'LikeController@like');

	Route::delete('/unlike/{id}', 'LikeController@unlike');

	Route::post('/report', 'ReportController@report');

	Route::get('/profile', 'QuizController@profile');

	Route::patch('/changeavatar', 'HomeController@avatar');
	
});

Route::middleware("admin")->group(function(){
	Route::get('/admin', 'ReportController@index');
	Route::patch('/banuser/{id}', 'ReportController@banuser');
	Route::get('/dashboard', 'ReportController@dashboard');
	Route::get('/categories', 'ReportController@categories');

	Route::delete('/deletecategory/{id}', 'CategoryController@destroy');
	Route::patch('/updatecategory/{id}', 'CategoryController@update');
	Route::post('/createcategory', 'CategoryController@create');

	Route::delete('/deleteviolation/{id}', 'ViolationController@destroy');
	Route::post('/createviolation', 'ViolationController@create');
	Route::patch('/updateviolation/{id}', 'ViolationController@update');
});








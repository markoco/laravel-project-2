-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2020 at 06:45 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cscmarch`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `answer`, `user_id`, `quiz_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, '2', 3, 2, 2, '2020-03-05 07:59:28', '2020-03-05 07:59:28'),
(2, '2', 2, 13, 2, '2020-03-05 08:50:27', '2020-03-05 08:50:27');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Math', NULL, '2020-03-03 22:59:23'),
(2, 'Logic', NULL, NULL),
(3, 'Grammar', NULL, NULL),
(4, 'Word Analogy', NULL, NULL),
(5, 'General Information', NULL, NULL),
(6, 'Science', '2020-03-03 23:10:38', '2020-03-03 23:10:38'),
(7, 'Other', '2020-03-03 23:11:22', '2020-03-03 23:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `quiz_id`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2020-03-05 07:58:05', '2020-03-05 07:58:05'),
(2, 3, 2, '2020-03-05 07:58:08', '2020-03-05 07:58:08'),
(3, 2, 13, '2020-03-05 08:49:59', '2020-03-05 08:49:59'),
(5, 2, 15, '2020-03-05 08:50:04', '2020-03-05 08:50:04'),
(6, 2, 14, '2020-03-05 08:52:26', '2020-03-05 08:52:26'),
(7, 1, 1, '2020-03-05 08:54:18', '2020-03-05 08:54:18'),
(8, 1, 3, '2020-03-05 08:54:23', '2020-03-05 08:54:23'),
(10, 1, 5, '2020-03-05 08:54:28', '2020-03-05 08:54:28'),
(11, 1, 6, '2020-03-05 08:54:32', '2020-03-05 08:54:32'),
(12, 1, 7, '2020-03-05 08:54:35', '2020-03-05 08:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_27_010326_create_roles_table', 1),
(5, '2020_02_27_010347_create_categories_table', 1),
(6, '2020_02_27_010355_create_quizzes_table', 1),
(7, '2020_02_27_010406_create_options_table', 1),
(8, '2020_02_27_010531_create_statuses_table', 1),
(9, '2020_02_27_010540_create_violations_table', 1),
(10, '2020_02_27_010552_create_reports_table', 1),
(11, '2020_02_27_010604_create_comments_table', 1),
(12, '2020_02_27_010612_create_likes_table', 1),
(13, '2020_02_27_010623_create_answers_table', 1),
(14, '2020_02_27_010714_add_role_to_user', 1),
(15, '2020_03_03_041239_create_quiz_violation_table', 1),
(16, '2020_03_04_015232_create_report_violation_table', 1),
(17, '2020_03_04_022938_add_status_to_user', 1),
(18, '2020_03_05_133744_add_avatar_to_user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option`, `quiz_id`, `created_at`, `updated_at`) VALUES
(1, '30', 1, '2020-03-05 07:47:00', '2020-03-05 07:47:00'),
(2, '3', 1, '2020-03-05 07:47:00', '2020-03-05 07:47:00'),
(3, '3.33', 1, '2020-03-05 07:47:00', '2020-03-05 07:47:00'),
(4, '20', 1, '2020-03-05 07:47:00', '2020-03-05 07:47:00'),
(5, '$48.00', 2, '2020-03-05 07:47:43', '2020-03-05 07:47:43'),
(6, '$51.94', 2, '2020-03-05 07:47:44', '2020-03-05 07:47:44'),
(7, '$251.94', 2, '2020-03-05 07:47:44', '2020-03-05 07:47:44'),
(8, '$299.94', 2, '2020-03-05 07:47:44', '2020-03-05 07:47:44'),
(9, '61.6 miles per hour', 3, '2020-03-05 07:48:06', '2020-03-05 07:48:06'),
(10, '84.0 miles per hour', 3, '2020-03-05 07:48:07', '2020-03-05 07:48:07'),
(11, '35.6 miles per hour', 3, '2020-03-05 07:48:07', '2020-03-05 07:48:07'),
(12, '71.1 miles per hour', 3, '2020-03-05 07:48:07', '2020-03-05 07:48:07'),
(17, 'The CEO travelled to the USA and UK last year.', 5, '2020-03-05 07:49:14', '2020-03-05 07:49:14'),
(18, 'The C.E.O travelled to the U.S.A and U.K last year.', 5, '2020-03-05 07:49:14', '2020-03-05 07:49:14'),
(19, 'The CEO travelled to the U.S.A. and U.K. last year.', 5, '2020-03-05 07:49:14', '2020-03-05 07:49:14'),
(20, 'The C.E.O. travelled to the USA and UK last year.', 5, '2020-03-05 07:49:14', '2020-03-05 07:49:14'),
(21, 'After seeing the lions at the zoo, we moved onto the monkeys.', 6, '2020-03-05 07:49:39', '2020-03-05 07:49:39'),
(22, 'All of his books turned into best sellers.', 6, '2020-03-05 07:49:39', '2020-03-05 07:49:39'),
(23, 'Put the biscuit in to the basket.', 6, '2020-03-05 07:49:39', '2020-03-05 07:49:39'),
(24, 'It takes upto eight hours to prepare a meal for that many people.', 6, '2020-03-05 07:49:39', '2020-03-05 07:49:39'),
(25, '3', 7, '2020-03-05 07:50:08', '2020-03-05 07:50:08'),
(26, '5', 7, '2020-03-05 07:50:08', '2020-03-05 07:50:08'),
(27, '2', 7, '2020-03-05 07:50:08', '2020-03-05 07:50:08'),
(28, '4', 7, '2020-03-05 07:50:08', '2020-03-05 07:50:08'),
(29, '1', 8, '2020-03-05 07:50:47', '2020-03-05 07:50:47'),
(30, '3', 8, '2020-03-05 07:50:47', '2020-03-05 07:50:47'),
(31, '4', 8, '2020-03-05 07:50:47', '2020-03-05 07:50:47'),
(32, '3', 8, '2020-03-05 07:50:47', '2020-03-05 07:50:47'),
(33, 'calendar', 9, '2020-03-05 07:51:18', '2020-03-05 07:51:18'),
(34, 'therefore', 9, '2020-03-05 07:51:18', '2020-03-05 07:51:18'),
(35, 'apparantly', 9, '2020-03-05 07:51:18', '2020-03-05 07:51:18'),
(36, 'appearance', 9, '2020-03-05 07:51:18', '2020-03-05 07:51:18'),
(37, 'May I have some pizza?', 10, '2020-03-05 07:51:54', '2020-03-05 07:51:54'),
(38, 'You can swim in this lake.', 10, '2020-03-05 07:51:55', '2020-03-05 07:51:55'),
(39, 'You may not leave the table.', 10, '2020-03-05 07:51:55', '2020-03-05 07:51:55'),
(40, 'Can I have some pizza?', 10, '2020-03-05 07:51:55', '2020-03-05 07:51:55'),
(41, 'The reign of Caesar was from 63 BC to AD 14.', 11, '2020-03-05 07:52:14', '2020-03-05 07:52:14'),
(42, 'The empire collapsed in AD 1054.', 11, '2020-03-05 07:52:14', '2020-03-05 07:52:14'),
(43, 'Did the new millenium begin in 2000 AD or 2001 AD?', 11, '2020-03-05 07:52:14', '2020-03-05 07:52:14'),
(44, 'The earliest recorded evidence is from 29 BC.', 11, '2020-03-05 07:52:15', '2020-03-05 07:52:15'),
(45, 'I can\'t be gone for long.', 12, '2020-03-05 07:52:35', '2020-03-05 07:52:35'),
(46, 'I cannot be gone for long.', 12, '2020-03-05 07:52:35', '2020-03-05 07:52:35'),
(47, 'I can not be gone for long.', 12, '2020-03-05 07:52:35', '2020-03-05 07:52:35'),
(48, 'She can not only run fast but has great stamina too.', 12, '2020-03-05 07:52:35', '2020-03-05 07:52:35'),
(49, 'TOAD : ORNITHOLOGY', 13, '2020-03-05 07:56:03', '2020-03-05 07:56:03'),
(50, 'TURTLE : MICROBIOLOGY', 13, '2020-03-05 07:56:03', '2020-03-05 07:56:03'),
(51, 'GYMNOSPERMS : BOTANY', 13, '2020-03-05 07:56:03', '2020-03-05 07:56:03'),
(52, 'FRIEND : HOME ECONOMICS', 13, '2020-03-05 07:56:03', '2020-03-05 07:56:03'),
(53, 'VIRTUAL : TRUTH', 14, '2020-03-05 07:56:29', '2020-03-05 07:56:29'),
(54, 'NORMAL : VALUE', 14, '2020-03-05 07:56:30', '2020-03-05 07:56:30'),
(55, 'MARGINAL : KNOWLEDGE', 14, '2020-03-05 07:56:30', '2020-03-05 07:56:30'),
(56, 'COINCIDENTAL : HEALTH', 14, '2020-03-05 07:56:30', '2020-03-05 07:56:30'),
(57, 'REPUTABLE : FAVORABLE', 15, '2020-03-05 07:56:56', '2020-03-05 07:56:56'),
(58, 'MATERNAL : UNFAVORABLE', 15, '2020-03-05 07:56:56', '2020-03-05 07:56:56'),
(59, 'DISPUTATIOUS : FAVORABLE', 15, '2020-03-05 07:56:56', '2020-03-05 07:56:56'),
(60, 'VIGILANT : UNFAVORABLE', 15, '2020-03-05 07:56:56', '2020-03-05 07:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@gmail.com', '$2y$10$B44rIF17oxHdn7mQUuqZQuQuFMtBIdFGoJ5SmZHKIiMmNnjFvEbDC', '2020-03-05 09:41:28');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answerKey` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `question`, `answerKey`, `user_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, '18 liters of water are poured into an aquarium that\'s 60cm long, 10cm wide, and 90cm high. How many cm will the water level in the aquarium rise due to this added water?\r\n\r\n(1 liter = 1,000cm3)', '4', 2, 1, '2020-03-05 07:46:59', '2020-03-05 08:00:55'),
(2, 'If Connie deposits $200 into a savings account with a 8% annual interest rate and doesn\'t withdraw any of the money for 3 years, what will her balance be at the end of 3 years?', '4', 2, 1, '2020-03-05 07:47:43', '2020-03-05 07:47:43'),
(3, 'If a car travels 462.2 miles in 6.5 hours what is its average speed?', '4', 2, 1, '2020-03-05 07:48:06', '2020-03-05 07:48:06'),
(5, 'Which one of these sentences is gramatically correct?', '4', 2, 3, '2020-03-05 07:49:14', '2020-03-05 07:49:14'),
(6, 'Which of the following sentences is gramatically correct?', '3', 2, 3, '2020-03-05 07:49:39', '2020-03-05 07:49:39'),
(7, '15 members of a bridal party need transported to a wedding reception but there are only 3 4-passenger taxis available to take them. How many will need to find other transportation?', '2', 2, 2, '2020-03-05 07:50:08', '2020-03-05 07:50:08'),
(8, 'How many of the following words are spelled correctly?\n\nenvironment,\nassassination,\nforty,\nfurther', '3', 2, 7, '2020-03-05 07:50:47', '2020-03-05 07:50:47'),
(9, 'Which of the following words is misspelled?', '3', 2, 7, '2020-03-05 07:51:18', '2020-03-05 07:51:18'),
(10, 'Which of the following sentences is not gramatically correct?', '3', 2, 3, '2020-03-05 07:51:54', '2020-03-05 07:51:54'),
(11, 'Which one of these sentences is not gramatically correct?', '4', 2, 3, '2020-03-05 07:52:14', '2020-03-05 07:52:14'),
(12, 'Which of the following sentences is not gramatically correct?', '3', 2, 3, '2020-03-05 07:52:35', '2020-03-05 07:52:35'),
(13, 'UNION JACK : VEXILLOLOGY', '4', 3, 4, '2020-03-05 07:56:03', '2020-03-05 07:56:03'),
(14, 'CHRONOLOGICAL : TIME', '4', 3, 4, '2020-03-05 07:56:29', '2020-03-05 07:56:29'),
(15, 'MORBID : UNFAVORABLE', '4', 3, 4, '2020-03-05 07:56:55', '2020-03-05 07:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_violation`
--

CREATE TABLE `quiz_violation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `violation_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `quiz_id` bigint(20) UNSIGNED NOT NULL,
  `status_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `user_id`, `quiz_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 2, 13, 1, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(2, 2, 14, 1, '2020-03-05 08:49:37', '2020-03-05 08:49:37'),
(3, 2, 15, 1, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(4, 2, 15, 1, '2020-03-05 08:49:55', '2020-03-05 08:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `report_violation`
--

CREATE TABLE `report_violation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `report_id` bigint(20) UNSIGNED NOT NULL,
  `violation_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `report_violation`
--

INSERT INTO `report_violation` (`id`, `report_id`, `violation_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(2, 1, 2, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(3, 1, 3, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(4, 1, 4, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(5, 1, 5, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(6, 1, 6, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(7, 1, 7, '2020-03-05 08:49:29', '2020-03-05 08:49:29'),
(8, 1, 8, '2020-03-05 08:49:30', '2020-03-05 08:49:30'),
(9, 2, 1, '2020-03-05 08:49:37', '2020-03-05 08:49:37'),
(10, 2, 2, '2020-03-05 08:49:37', '2020-03-05 08:49:37'),
(11, 2, 3, '2020-03-05 08:49:37', '2020-03-05 08:49:37'),
(12, 2, 4, '2020-03-05 08:49:38', '2020-03-05 08:49:38'),
(13, 2, 5, '2020-03-05 08:49:38', '2020-03-05 08:49:38'),
(14, 2, 6, '2020-03-05 08:49:38', '2020-03-05 08:49:38'),
(15, 2, 7, '2020-03-05 08:49:38', '2020-03-05 08:49:38'),
(16, 2, 8, '2020-03-05 08:49:38', '2020-03-05 08:49:38'),
(17, 3, 1, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(18, 3, 2, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(19, 3, 3, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(20, 3, 4, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(21, 3, 5, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(22, 3, 6, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(23, 3, 7, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(24, 3, 8, '2020-03-05 08:49:48', '2020-03-05 08:49:48'),
(25, 4, 1, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(26, 4, 2, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(27, 4, 3, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(28, 4, 4, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(29, 4, 5, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(30, 4, 6, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(31, 4, 7, '2020-03-05 08:49:56', '2020-03-05 08:49:56'),
(32, 4, 8, '2020-03-05 08:49:56', '2020-03-05 08:49:56');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL),
(2, 'User', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Correct', NULL, NULL),
(2, 'Incorrect', NULL, NULL),
(3, 'Pending', NULL, NULL),
(4, 'Close', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT 2,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/avatar11.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `status`, `avatar`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$UerIpF.7aAR5S/wSb46emu.EVDcubzVi/B/a5EwpuL81X/mnjqaqa', NULL, '2020-03-05 07:44:01', '2020-03-05 08:53:57', 1, 'Active', 'images/avatar10.png'),
(2, 'user', 'user@gmail.com', NULL, '$2y$10$Ix11/TXwNfx3TYLo4XM3gebtBJZ8l7r6p14ahNPUuxgWyK/AIa0u.', NULL, '2020-03-05 07:44:42', '2020-03-05 07:44:42', 2, 'Active', 'images/avatar11.png'),
(3, 'user2', 'user2@gmail.com', NULL, '$2y$10$x3BFZEhW.gu1jt19DdBZy.UTPBWhLWm5J88YfvggZYhUiSOJ1M4Y.', NULL, '2020-03-05 07:54:31', '2020-03-05 07:59:15', 2, 'Active', 'images/avatar3.png'),
(4, 'admin2', 'admin2@gmail.com', NULL, '$2y$10$4vjZVpXw04FEhHuOneEcfeOr0YQZINbfOZK4toX9MK.KWuRAqmMoG', NULL, '2020-03-05 09:44:36', '2020-03-05 09:44:36', 1, 'Active', 'images/avatar11.png');

-- --------------------------------------------------------

--
-- Table structure for table `violations`
--

CREATE TABLE `violations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `violation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `violations`
--

INSERT INTO `violations` (`id`, `violation`, `created_at`, `updated_at`) VALUES
(1, 'Profanity', NULL, NULL),
(2, 'Racism', NULL, NULL),
(3, 'Misspelling', NULL, NULL),
(4, 'Poorly Constructed', NULL, NULL),
(5, 'Incorrect Question', NULL, NULL),
(6, 'Localization', NULL, NULL),
(7, 'Irrelevant', NULL, '2020-03-03 23:42:32'),
(8, 'Other', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answers_user_id_foreign` (`user_id`),
  ADD KEY `answers_quiz_id_foreign` (`quiz_id`),
  ADD KEY `answers_status_id_foreign` (`status_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_quiz_id_foreign` (`quiz_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `likes_user_id_foreign` (`user_id`),
  ADD KEY `likes_quiz_id_foreign` (`quiz_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_quiz_id_foreign` (`quiz_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quizzes_user_id_foreign` (`user_id`),
  ADD KEY `quizzes_category_id_foreign` (`category_id`);

--
-- Indexes for table `quiz_violation`
--
ALTER TABLE `quiz_violation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quiz_violation_quiz_id_foreign` (`quiz_id`),
  ADD KEY `quiz_violation_violation_id_foreign` (`violation_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_user_id_foreign` (`user_id`),
  ADD KEY `reports_quiz_id_foreign` (`quiz_id`),
  ADD KEY `reports_status_id_foreign` (`status_id`);

--
-- Indexes for table `report_violation`
--
ALTER TABLE `report_violation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_violation_report_id_foreign` (`report_id`),
  ADD KEY `report_violation_violation_id_foreign` (`violation_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `violations`
--
ALTER TABLE `violations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `quiz_violation`
--
ALTER TABLE `quiz_violation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `report_violation`
--
ALTER TABLE `report_violation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `violations`
--
ALTER TABLE `violations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answers_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `answers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD CONSTRAINT `quizzes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quizzes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `quiz_violation`
--
ALTER TABLE `quiz_violation`
  ADD CONSTRAINT `quiz_violation_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `quiz_violation_violation_id_foreign` FOREIGN KEY (`violation_id`) REFERENCES `violations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reports_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `report_violation`
--
ALTER TABLE `report_violation`
  ADD CONSTRAINT `report_violation_report_id_foreign` FOREIGN KEY (`report_id`) REFERENCES `reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_violation_violation_id_foreign` FOREIGN KEY (`violation_id`) REFERENCES `violations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

//Javascript for Fetch
let selectedKeyAnswer = "";
let selectedCategoryId = "";
let txtQuestion = document.getElementById('question');
let txtOption1 = document.getElementById('option1');
let txtOption2 = document.getElementById('option2');
let txtOption3 = document.getElementById('option3');
let txtOption4 = document.getElementById('option4');
let txtKeyAnswer = document.getElementById('keyAnswer');
let txtCategoryId = document.getElementById('categoryId');

// To Save Quiz
const postQuiz = ()=>{
	let options = [];
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let question = txtQuestion.value;
	let option1 = txtOption1.value;
	let option2 = txtOption2.value;
	let option3 = txtOption3.value;
	let option4 = txtOption4.value;
	let keyAnswer = txtKeyAnswer.value;
	let categoryId = txtCategoryId.value;
	selectedKeyAnswer = keyAnswer-1;
	selectedCategoryId = categoryId-1;

	options.push(option1,option2,option3,option4);

	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("question", question);
	data.append("1", option1);
	data.append("2", option2);
	data.append("3", option3);
	data.append("4", option4);
	data.append("keyAnswer", keyAnswer);
	data.append("categoryId", categoryId);
    


	if(question == "" || option1 == "" || option2 == "" || option3 == "" || option4 == "" || keyAnswer =="" || categoryId == ""){
		 Swal.fire({
			 icon: 'error',
			 title: 'Oops...',
			 text: 'Please Complete All the Details!'
		});
	}else{

		Swal.fire({
		  title: 'Your quiz has been posted',
		  showClass: {
		    popup: 'animated fadeInDown faster'
		  },
		  hideClass: {
		    popup: 'animated fadeOutUp faster'
		  }
		});
		$('#createQuizModal').modal('hide');
		clearQuizForm();
		fetch("/createquiz",{
			method: "POST",
			body: data
		});
	}

}

//To Submit Answer 
const answerBtn = (id,key)=>{
	let validateAnswer = document.getElementById('validateAnswer'+id);
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let answer = document.querySelector('input[name=optionRadio'+id+']:checked');
	// console.log('answer'+answer)
	let answerKeyText = document.getElementsByName('optionRadio'+id);
	answerKeyText = answerKeyText[key-1].nextElementSibling.textContent;
	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("answerId",  parseInt(answer.value)+1);
	data.append("answerKey", key);
	data.append("quizId", id);
	if(answer.value == 'x'){
		 Swal.fire({
			 icon: 'error',
			 title: 'Oops...',
			 text: 'Please Select an Answer!'
		});
	}else{
		let btnSubmit = document.getElementById("btnSubmit"+id);
		document.getElementById("btnSubmit"+id).style = "display:none";
		validateAnswer.style.color = "green";
		validateAnswer.textContent = "Correct Answer: "+answerKeyText;
		console.log((parseInt(answer.value))+" "+key)
		if(parseInt(answer.value)+1 == key){
			Swal.fire({
			  position: 'center',
			  icon: 'success',
			  title: 'You Got It!',
			  showConfirmButton: false,
			  timer: 1500
			});
		}
		else{
			validateAnswer.style.color = "red";
			validateAnswer.textContent = "Correct Answer is: "+answerKeyText;
			
			Swal.fire({
			  position: 'center',
			  icon: 'error',
			  title: 'Incorrect!',
			  showConfirmButton: false,
			  timer: 1500
			});
		}
		fetch("/submitanswer",{
			method: "POST",
			body: data
		});	
	}	
}

const deleteQuiz = (id)=>{
	const csrfToken = document.querySelector("[name='csrf-token']").content

	Swal.fire({
	  title: 'Are you sure? ',
	  text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
		let data = new FormData;
		data.append("_token", csrfToken);
		data.append("_method", "DELETE")
		fetch("/deletequiz/"+id,{
			method: "POST",
			body: data
		}).then(res => res.text())
		.then(res=>{
			const quizForm = document.querySelector("#quizForm"+id);
			quizForm.classList.add("d-none");
		})

    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    );
  }
});
}

const likeQuiz = id=>{
		const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
		let data = new FormData;
		data.append("_token", csrfToken);
		data.append("quizId", id);

		fetch("/like",{
			method: "POST",
			body: data
		});	
}

const unlikeQuiz = id=>{
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("quizId", id);

	fetch("/unlike/"+id,{
		method: "DELETE",
		body: data
	});	
}

const reportQuiz = id=>{
		alert(violations);
		const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
		let data = new FormData;
		data.append("_token", csrfToken);
		data.append("quizId", id);
		data.append("violations", violations);
		fetch("/report/"+id,{
			method: "POST",
			body: data
		});	
}



const banUser = (id,userId,quizId)=>{
	let reportRow = document.getElementById('row'+id);
	let user = reportRow.lastElementChild.previousElementSibling.previousElementSibling.textContent;
	Swal.fire({
	  title: 'Are you sure?',
	  text: "Proceed",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes!'
	}).then((result) => {
	  if (result.value) {
	    Swal.fire(
	      'Update!',
	      'User status has been updated.',
	      'success'
	    )
	    const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	    let data = new FormData;
		data.append("_token", csrfToken);
		data.append("_method", "PATCH")
		fetch("/banuser/"+userId,{
			method: "POST",
			body: data
		}).then(res => res.text())
		.then(res=>{
			location.reload();
		})
	  }
	})
}


const deleteQuizAdmin = (id,userId,quizId)=>{
	
		const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	    let data = new FormData;
		data.append("_token", csrfToken);
		data.append("_method", "DELETE")
		fetch("/deletequiz/"+quizId,{
			method: "POST",
			body: data
		}).then(res => res.text())
		.then(res=>{
			// location.reload();
			console.log(quizId)
		})
	
}

const deleteCategory = (id)=>{
	Swal.fire({
	  title: 'Are you sure?',
	  text: "Delete Category ",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, Delete category!'
	}).then((result) => {
	  if (result.value) {
	    Swal.fire(
	      'Deleted!',
	      'Your file has been deleted.',
	      'success'
	    )
	    const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	    let data = new FormData;
		data.append("_token", csrfToken);
		data.append("_method", "DELETE")
		fetch("/deletecategory/"+id,{
			method: "POST",
			body: data
		}).then(res => res.text())
		.then(res=>{
			// location.reload();
			document.getElementById("tr"+id).classList.add('d-none');
		})
	  }
	})
}

const editCategory = (id,name)=>{
	document.getElementById("categoryBody").classList.add('d-none');
	document.getElementById("categoryUpdate").classList.remove('d-none');
	document.getElementById("backToCategory").classList.remove('d-none');
	document.getElementById("categoryName").value = document.getElementById('tr'+id).firstElementChild.textContent;
	document.getElementById("categoryId").value = id;
}

function backToCategory(){
	document.getElementById("categoryBody").classList.remove('d-none');
	document.getElementById("categoryUpdate").classList.add('d-none');
	document.getElementById("backToCategory").classList.add('d-none');
	document.getElementById("categoryName").value = "";
	document.getElementById("categoryId").value = "";
}

function updateCategory(){
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let categoryName = document.getElementById("categoryName").value
	let id = document.getElementById("categoryId").value
	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("_method", "PATCH");
	data.append("categoryName", categoryName);
	fetch("/updatecategory/"+id,{
		method: "POST",
		body: data
	}).then(res => res.text())
	.then(res=>{
		document.getElementById('tr'+id).firstElementChild.textContent = categoryName;
		backToCategory();
	
	})
}

const addNewCategory = ()=>{
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let categoryName = document.getElementById("newCategory").value
	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("categoryName", categoryName);
	fetch("/createcategory",{
		method: "POST",
		body: data
	}).then(res => res.text())
	.then(res=>{
		Swal.fire({
			position: 'center',
			icon: 'success',
			title: 'Your work has been saved',
			showConfirmButton: false,
			timer: 1500
		  })
		location.reload();
	})
}

const deleteViolation = (id)=>{
	Swal.fire({
	  title: 'Are you sure?',
	  text: "Delete Category ",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, Delete category!'
	}).then((result) => {
	  if (result.value) {
	    Swal.fire(
	      'Deleted!',
	      'Your file has been deleted.',
	      'success'
	    )
	    const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	    let data = new FormData;
		data.append("_token", csrfToken);
		data.append("_method", "DELETE")
		fetch("/deleteviolation/"+id,{
			method: "POST",
			body: data
		}).then(res => res.text())
		.then(res=>{
			// location.reload();
			document.getElementById("trViolation"+id).classList.add('d-none');
		})
	  }
	})
}

const addNewViolation = ()=>{
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let violationName = document.getElementById("newViolation").value
	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("violationName", violationName);
	fetch("/createviolation",{
		method: "POST",
		body: data
	}).then(res => res.text())
	.then(res=>{
		Swal.fire({
			position: 'center',
			icon: 'success',
			title: 'Your work has been saved',
			showConfirmButton: false,
			timer: 1500
		  })
		location.reload();
	})
}

const editViolation = (id)=>{
	document.getElementById("violationBody").classList.add('d-none');
	document.getElementById("violationUpdate").classList.remove('d-none');
	document.getElementById("backToViolation").classList.remove('d-none');
	document.getElementById("violationName").value = document.getElementById('trViolation'+id).firstElementChild.textContent;
	document.getElementById("violationId").value = id;
}

function backToViolation(){
	document.getElementById("violationBody").classList.remove('d-none');
	document.getElementById("violationUpdate").classList.add('d-none');
	document.getElementById("backToViolation").classList.add('d-none');
	document.getElementById("violationName").value = "";
	document.getElementById("violationId").value = "";
}

function updateViolation(){
	const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content
	let violationName = document.getElementById("violationName").value
	let id = document.getElementById("violationId").value
	let data = new FormData;
	data.append("_token", csrfToken);
	data.append("_method", "PATCH");
	data.append("violationName", violationName);
	fetch("/updateviolation/"+id,{
		method: "POST",
		body: data
	}).then(res => res.text())
	.then(res=>{
		document.getElementById('trViolation'+id).firstElementChild.textContent = violationName;
		backToViolation();
	
	})
}


//Javascript for Designs

// To Clear Quiz Form
function clearQuizForm(){
	let btnCheck = document.querySelectorAll('.btnCheck');
	btnCheck[selectedKeyAnswer].classList.remove('text-success');
	btnCheck[selectedKeyAnswer].nextElementSibling.classList.remove('is-valid');

	let badgeCategory = document.querySelectorAll('.badgeCategory');
	badgeCategory[selectedCategoryId].style = "filter: opacity(100%);";

	txtQuestion.value = "";
	txtOption1.value = "";
	txtOption2.value = "";
	txtOption3.value = "";
	txtOption4.value = "";
	txtKeyAnswer.value = "";
	txtCategoryId.value = "";
}


//To Select Violations
let violations = [];
let badgeReport = document.querySelectorAll('.badgeReport');
badgeReport.forEach((element,i)=>{
	let selected = false;
    badgeReport[i].addEventListener('click',()=>{
		var violationId = badgeReport[i].getAttribute("data-id");
        if (selected == false) {
            badgeReport[i].style = "filter: opacity(30%);"
            selected = true;
            violations.push(violationId);       
        }
        else {
            badgeReport[i].style = "filter: opacity(100%);"
            selected = false;
            violations = violations.filter(item => item !== violationId)
        }
        console.log(violations);   
    });
});

//For Quiz Categories
let badgeCategory = document.querySelectorAll('.badgeCategory');
badgeCategory.forEach((category,i)=>{
	let selected = false;
	console.log(i)
    badgeCategory[i].addEventListener('click',()=>{
        if (selected == false) {
            badgeCategory[i].style = "filter: opacity(30%);"
            selected = true;        
        }
        else {
            badgeCategory[i].style = "filter: opacity(100%);"
            selected = false;
        }
        badgeCategory.forEach((categoryElem, j)=>{
            if(i==j){
                document.getElementById('categoryId').value = i+1;
                badgeCategory[j].style = "filter: opacity(30%);"
            }else{
                badgeCategory[j].style = "filter: opacity(100%);"
            }
        });
        
    });
});

//For Selecting Key Answer
let btnCheck = document.querySelectorAll('.btnCheck');
btnCheck.forEach((element,i)=>{
    btnCheck[i].addEventListener('click',()=>{
        btnCheck.forEach((elem,j)=>{
            if(i == j){
                btnCheck[j].classList.add('text-success');
                btnCheck[j].nextElementSibling.classList.add('is-valid');
                document.getElementById('keyAnswer').value = i+1;
            }else{
                btnCheck[j].classList.remove('text-success');
                btnCheck[j].nextElementSibling.classList.remove('is-valid'); 
            }
        });
        
    });
});

//To Display Report Div
const showReportDiv = id=>{
	let quizForm = document.getElementById('quizForm'+id);
	quizForm.classList.add('invisible');
	quizForm.nextElementSibling.classList.remove('d-none');
}

//To Close Report Div
const closeReportBtn = id=>{
	let quizForm = document.getElementById('quizForm'+id);
	quizForm.classList.remove('invisible');
	quizForm.nextElementSibling.classList.add('d-none');
	clearSelectedReports();
	
}

function clearSelectedReports(){
	violations = [];
	badgeReport.forEach((element,i)=>{
		selected = false;
		badgeReport[i].style = "filter: opacity(100%);"
	});
	console.log(violations);
}

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();   
});

// For Avatar
let avatar = document.querySelectorAll('.avatar');
avatar.forEach((img,i)=>{
	let selected = false;
	console.log(i)
    avatar[i].addEventListener('click',()=>{
        if (selected == false) {
			avatar[i].style = "filter: scale(75%); background-color:blue; border-radius:50%;";
			document.getElementById('btnChangeAvatar').classList.remove('d-none');
            selected = true;        
        }
        else {
           	avatar[i].style = "filter: scale(0%); background-color:transparent; border-radius:0%;";
            selected = false;
        }
        avatar.forEach((imgElem, j)=>{
            if(i==j){
				//asset('images/avatar1.png')
                document.getElementById('txtAvatar').value = 'images/avatar'+i+'.png';
				avatar[j].style = "filter: scale(75%); background-color:blue; border-radius:50%;";
				document.getElementById('btnChangeAvatar').classList.remove('d-none');
            }else{
                avatar[j].style = "filter: scale(0%); background-color:transparent; border-radius:0%;";
            }
        });
        
    });
});



//  fetch('/admin')
//   .then((response) => {
//     return response.json();
//   })
//   .then((data) => {
// 	let x = data.data;
// 	console.log(x);
// 	let html = "";
// 	// x.forEach(d => {
// 	// 	html+= `
// 	// 	<tr id="row${d.id}">
// 	// 		<td>${d.id}</td>
// 	// 		<td>${d.quiz_id}</td>
// 	// 		<td> </td>
// 	// 		<td> </td>
// 	// 		<td> </td>
// 	// 		<td> </td>
// 	// 	</tr>
// 	// 	`;
// 	// });
// 	document.getElementById('test').innerHTML = html;
//   });
		



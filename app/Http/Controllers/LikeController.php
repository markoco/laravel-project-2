<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use\App\Like;
class LikeController extends Controller
{
    public function like($id, Request $req){
        $like = new Like;
        $like->quiz_id = $id;
        $like->user_id = Auth::user()->id;
        $like->save();
        return redirect()->back();
    }
    public function unlike($id){

        $itemToDelete = Like::find($id);
        $itemToDelete->delete();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Violation;
class ViolationController extends Controller
{
    public function destroy($id){
        $violationToDelete = Violation::find($id)->delete();
        return redirect()->back();
    }
    public function create(Request $req){
        $violation = new Violation;
        $violation->violation = $req->violationName;
        $violation->save();
        return redirect()->back();
    }
    public function update($id, Request $req){
        $violation = Violation::find($id);
        $violation->violation = $req->violationName;
        $violation->save();
        return redirect()->back();
    }
}

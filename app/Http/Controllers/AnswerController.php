<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Category;
use\App\Quiz;
use\App\Violation;
use\App\Answer;
use Auth;
use\App\Like;
class AnswerController extends Controller
{
    public function submitanswer(Request $req){
    	$categories = Category::all();
    	$quizzes = Quiz::all();
        $violations = Violation::all();
        $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        $score = "";
        if(count($allAnswer)==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/count($allAnswer)*100; 
        }
        $answer = new Answer;
        $answer->answer = $req->answerId;
        $answer->user_id = Auth::user()->id;
        $answer->quiz_id = $req->quizId;

        $status = "";
        if($req->answerId == $req->answerKey){
            $status = 1;
        }else{
            $status = 2;
        }

        $answer->status_id = $status;
        $answer->save();
        return view('userviews.review',compact('allAnswer','countCorrectAnswer','score','likes','categories','answers','violations'));

        // return view('userviews.landingpage', compact('categories','quizzes','violations','score','allAnswer'));
    }

    public function reviewansweredquiz(){
        $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        $score = "";
        if(count($allAnswer)==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/count($allAnswer)*100; 
        }
        $answers = Answer::all();
        $categories = Category::all();
        $likes = Like::all();
        $violations = Violation::all();
        // $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        return view('userviews.review',compact('allAnswer','countCorrectAnswer','score','likes','categories','answers','violations'));
    }

}

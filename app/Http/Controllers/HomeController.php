<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use\App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function avatar(Request $req){
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->avatar = $req->avatarName;
        $user->save();

        return redirect()->back();

    }
}

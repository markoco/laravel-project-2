<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use\App\Category;
use\App\Quiz;
use\App\Violation;
use\App\Answer;
use\App\Option;
use\App\Like;
use\App\User;
use Auth;
class QuizController extends Controller
{
    public function index(){
        if(Auth::user()->status == 'Active'){
            if(Auth::user()->role_id == 2){
                $categories = Category::all();
            	$quizzes = Quiz::all();
                $violations = Violation::all();
                $allLikes = Like::all();
                $likes = Like::where('user_id','=',Auth::user()->id)->get();
                $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
                $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
                $score = "";
                $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
                $numLike = DB::table('likes')
                ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
                ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
                if(count($allAnswer)==0){
                    $score="NA";
                }else{
                    $score = $countCorrectAnswer/count($allAnswer)*100; 
                }
            	return view('userviews.landingpage', compact('countCorrectAnswer','numQuiz','numLike','likes','categories','quizzes','violations','score','allAnswer','allLikes'));
            }else{
                return redirect('/dashboard');
            }
        }else{
                 return view('404');
        }
    }
      

    public function createquiz(Request $req){
        // dd($req);
    	// $categories = Category::all();
        // $quizzes = Quiz::all();
        // $likes = Like::all();
        // $violations = Violation::all();
        // $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        // $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        // $score = "";
        // if(count($allAnswer)==0){
        //     $score="NA";
        // }else{
        //     $score = $countCorrectAnswer/count($allAnswer)*100; 
        // }

        $categories = Category::all();
        $quizzes = Quiz::all();
        $violations = Violation::all();
        $likes = Like::where('user_id','=',Auth::user()->id)->get();
        $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        $score = "";
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
        if(count($allAnswer)==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/count($allAnswer)*100; 
        }
     
    	$quiz = new Quiz;
        $quiz->question = $req->question;
        $quiz->answerKey = $req->keyAnswer;
        $quiz->user_id = Auth::user()->id;
        $quiz->category_id = $req->categoryId;
        $quiz->save();

        $insertedId = $quiz->id;
        for($i=0; $i<5; $i++){
            if(isset($req->$i)){
                $option = new Option;
                $option->option = $req->$i;
                $option->quiz_id = $insertedId;
                $option->save();
            }
        }    
        return view('userviews.landingpage', compact('countCorrectAnswer','numQuiz','numLike','likes','categories','quizzes','violations','score','allAnswer'));

    }
    // SELECT likes.quiz_id,quizzes.user_id FROM likes JOIN quizzes ON(quizzes.id = likes.quiz_id)

    public function myquizzes(){
        $quizzes = Quiz::where('user_id',Auth::user()->id)->get();
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
        $likes= Like::all();
        $categories = Category::all();
        return view('userviews.profile',compact('quizzes','categories','numQuiz','numLike','likes'));
    }

    public function deletequiz($id){
        $quizToDelete = Quiz::find($id)->delete();
        return redirect()->back();
    }

    public function updatequiz($id){
        $categories = Category::all();
        $quiz = Quiz::find($id);
        return view('userviews.updatequiz',compact('categories','quiz'));
    }

    public function editquiz($id, Request $req){
        $quiz = Quiz::find($id);
        $quiz->question = $req->question;
        $quiz->answerKey = $req->keyAnswer;
        $quiz->category_id = $req->categoryId;
        $quiz->save();

       
        for($i=0; $i<5; $i++){
            $opt = ('option' . $i);
            if(isset($req->$i)){
                $option = Option::find($req->$i);
                $option->option = $req->$opt;
                $option->quiz_id = $id;
                $option->save();
            }
        }  
        return redirect('/myquizzes');
    }

    public function filter($id){
        $categories = Category::all();
        $likes = Like::where('user_id','=',Auth::user()->id)->get();
    	$quizzes = Quiz::where('user_id','!=',Auth::user()->id)->where('category_id',$id)->get();
        $violations = Violation::all();
        $violations = Violation::all();
        $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        $score = "";
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
        $score = "";
        if(count($allAnswer)==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/count($allAnswer)*100; 
        }
        return view('userviews.landingpage', compact('countCorrectAnswer','numQuiz','numLike','likes','categories','quizzes','violations','score','allAnswer'));        // $items = Item::where('category_id', $id)->get();
        // $categories = Category::all();
        // return view('catalog', compact('items','categories'));
    }

    public function search(Request $req){
        $categories = Category::all();
        $likes = Like::where('user_id','=',Auth::user()->id)->get();
        $quizzes= Quiz::where('question', 'LIKE', '%' . $req->search .'%')->get();
        $violations = Violation::all();
        $allAnswer = Answer::where('user_id', Auth::user()->id)->get();
        $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        $score = "";
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
        $score = "";
        if(count($allAnswer)==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/count($allAnswer)*100; 
        }
        return view('userviews.landingpage', compact('countCorrectAnswer','numQuiz','numLike','likes','categories','quizzes','violations','score','allAnswer'));        // $items = Item::where('category_id', $id)->get();
    }

    public function filterprofile($id){
        $quizzes = Quiz::where('user_id',Auth::user()->id)->where('category_id',$id)->get();
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
       
        $categories = Category::all();
        return view('userviews.profile',compact('quizzes','categories','numQuiz','numLike'));
    }

    public function searchprofile(Request $req){
        $quizzes = Quiz::where('user_id',Auth::user()->id)->where('question', 'LIKE', '%' . $req->search .'%')->get();
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
       
        $categories = Category::all();
        return view('userviews.profile',compact('quizzes','categories','numQuiz','numLike'));
    }

    public function profile(){
        $numQuiz = Quiz::where('user_id',Auth::user()->id)->count();
        $allAnswer = Answer::where('user_id', Auth::user()->id)->count();
        $countCorrectAnswer = Answer::where('user_id', Auth::user()->id)->where('status_id',1)->count();
        $score = "";
        if($allAnswer==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/($allAnswer)*100; 
        }
        $numLike = DB::table('likes')
        ->join('quizzes', 'quizzes.id', '=', 'likes.quiz_id')
        ->select('likes.quiz_id', 'quizzes.user_id')->where('quizzes.user_id','=',Auth::user()->id)->count();
        return view('userviews.about', compact('numQuiz','numLike','countCorrectAnswer','allAnswer','score'));
    }
    


    
   
 

}

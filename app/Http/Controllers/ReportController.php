<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Violation;
use\App\Report;
use\App\Quiz;
use\App\User;
use\App\Like;
use\App\Category;
use\App\Answer;
use Auth;
class ReportController extends Controller
{
	public function index(){
		$reports = Report::paginate(6);
        // return (view('adminviews.reports', compact('reports'));)
        return($reports);
	}

    public function report(Request $req){
	    $report= new Report;
        $report->user_id = Auth::user()->id;
        $report->quiz_id = $req->quizId;
        $report->status_id = 1;
        $report->save();

        $violations = Violation::all();
        foreach ($violations as $violation) {
                $id = $violation->id;
                if(isset($req->$id)){
                    $report->violations()->attach($id);
                }
        }

        return redirect()->back();
    }

    public function banuser($id){
        $user = User::find($id);
        if($user->status == 'Active'){
           $user->status = 'Banned'; 
        }else{
            $user->status = 'Active'; 
        }
        $user->save();
    }

    public function dashboard(){
        $reports = Report::paginate(10);
        $categories =  Category::all();
        $violations =  Violation::all();
        $numQuiz =  Quiz::all()->count();
        $likes = Like::all()->count();
        $users = User::all()->count();
        $allAnswer = Answer::all();
        $countCorrectAnswer = Answer::where('status_id',1)->count();
        $score = "";
        if(count($allAnswer)==0){
            $score="NA";
        }else{
            $score = $countCorrectAnswer/count($allAnswer)*100; 
        }
        return view('adminviews.dashboard', compact('numQuiz','likes','users','reports','score','categories','violations'));
    }

    public function categories(){
        $categories =  Category::all();
        $violations =  Violation::all();
        return view('adminviews.categories',compact('categories','violations'));
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Category;
class CategoryController extends Controller
{
    public function destroy($id){
        $categoryToDelete = Category::find($id)->delete();
        return redirect()->back();
    }

    public function update($id, Request $req){
        $category = Category::find($id);
        $category->category = $req->categoryName;
        $category->save();
        return redirect()->back();
    }
    public function create(Request $req){
        $category = new Category;
        $category->category = $req->categoryName;
        $category->save();
        return redirect()->back();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function option(){
    	return $this->hasMany("\App\Option","quiz_id");
    }
    public function answer(){
    	return $this->hasMany("\App\Answer","quiz_id");
    }
    public function user(){
    	return $this->belongsTo("\App\User");
    }
    public function like(){
    	return $this->hasMany("\App\Like");
    }
    public function violations(){
    	return $this->belongsToMany('\App\Violation')->withTimeStamps();
    }
   
}

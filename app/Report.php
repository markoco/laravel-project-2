<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     public function violations(){
    	return $this->belongsToMany("\App\Violation")->withTimeStamps();
    }
    public function user(){
    	return $this->belongsTo("\App\User");
    }
    public function quiz(){
    	return $this->belongsTo("\App\Quiz");
    }
}

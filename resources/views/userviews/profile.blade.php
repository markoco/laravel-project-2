@extends('layouts.app')
@php ($badgeColor = array('badge-primary','badge-secondary','badge-success','badge-danger','badge-warning','badge-info','badge-dark'))
@section('side')
<div class="text-center">
    <div class="d-flex flex-column">
        <div class="card mb-3 p-2 bg-info">
            <img src="{{asset(Auth::user()->avatar)}}" id="avatarImg" height="75px" width="75px" class="mx-auto"  data-toggle="modal" data-target="#avatarModal">
            <ul class="text-left mt-2 text-white list-unstyled px-3">
                <li>User Id: <span class="text-white">{{Auth::user()->id}}</span></li>
                <li>Name: <span class="text-white">{{strtoupper(trans(Auth::user()->name))}}</span></li>
                <li>Email: <span class="text-white">{{Auth::user()->email}}</span></li>
            </ul>
        </div>
        <div class="card mb-3 bg-light">
            <div class="card-body">
                <h4 class="card-title text-left"><span class="text-warning">STARS</span> Received</h4>
            <h1>{{$numLike}}</h1>
            </div>
        </div>
        <div class="card mb-3 bg-light">
            <div class="card-body">
                <h4 class="card-title text-left"><span class="text-success">QUIZ</span> Posted</h4>
                <h1>{{$numQuiz}}</h1>
            </div>
        </div>
    </div>
 <hr>
 <p class="text-left lead text-dark">Filter by Category</p>
 @foreach ($categories as $category)
     <a href="/quizprofile/{{$category->id}}" class="badgeCategory2 badge badge-pill {{$badgeColor[array_rand($badgeColor)]}} m-1 text-white">{{$category->category}}</a>
 @endforeach
   
</div>
@endsection
@section('content')
<div class="row">
	<div class="col-lg-12 px-5">
        <form action="/searchprofile"  method="POST">
            @csrf
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search quiz by question">
                <div class="input-group-append">
                    <button class="btn btn-info">Search</button>
                </div>
            </div>  
        </form>
        @foreach($quizzes as $quiz)
                <div id="quizForm{{$quiz->id}}" class="bg-light p-5 my-2 position-relative">
    {{-- Quiz Div --}}
                    <div  class="quizForm">
                        <div>
                            <span class="text-secondary">Posted by: {{ucfirst(Auth::user()->name)}}</span>                     
                        </div>
                        <h5 class="my-3">{{$quiz->question}}</h5>
                        <div class="form-group">
                        	<ul>
                                @foreach($quiz->option as $key => $option)
                                    @if($quiz->answerKey == $key+1)
                                        <b><li class="text-success">{{$option->option}}</li></b>
                                    @else
                                        <li>{{$option->option}}</li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <p class="text-secondary text-left">{{$quiz->created_at->diffForHumans()}}</p>
                        <div class="text-right">
                            <span class="badgeBtn badge badge-pill px-3 badge-warning mx-2" onclick="deleteQuiz({{$quiz->id}})">Delete</span>
                            <a href="/updatequiz/{{$quiz->id}}">
                                <span class="badgeBtn badge badge-pill px-3 badge-success">Update</span>
                            </a>
                        </div>
                        {{-- <a href="/updatequiz/{{$quiz->id}}"> 
                            <i class="updateIcon fas fa-pen-square text-info" title="Update"></i>
                        </a>
                        <i class="deleteIcon fas fa-trash-alt text-danger" title="Delete" onclick="deleteQuiz({{$quiz->id}})"></i>    --}}
                    </div>
                </div>
        @endforeach
    </div>
</div>

{{-- Choose Avatar --}}
<div class="modal" id="avatarModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Change Profile Picture</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div>
            <img src="{{asset('images/avatar0.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar1.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar2.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar3.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar4.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar5.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar6.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar7.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar8.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar9.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar10.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
            <img src="{{asset('images/avatar11.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          </div>
        </div>
        <div class="modal-footer">
          <form action="/changeavatar" method="POST">
            @csrf
            @method('PATCH')
            <input type="hidden" name="avatarName" id="txtAvatar" >
            <button type="submit" class="btn btn-primary d-none" id="btnChangeAvatar">Save changes</button>
          </form>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endsection
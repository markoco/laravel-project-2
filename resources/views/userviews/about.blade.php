@extends('layouts.app')
@section('content')
<div class="row px-5">
    <div class="col-lg-7 p-5 bg-light card">
        <img src="{{asset(Auth::user()->avatar)}}" id="avatarImg" height="75px" width="75px" class="ml-5 mb-4"  data-toggle="modal" data-target="#avatarModal">
        <h5>User Id: <span class="text-muted">{{Auth::user()->id}}</span></h5>
        <h5>Name: <span class="text-muted">{{strtoupper(trans(Auth::user()->name))}}</span></h5>
        <h5>Email: <span class="text-muted">{{Auth::user()->email}}</span></h5>
        <h5>Account Status: <span class="text-success">{{Auth::user()->status}}</span></h5>
        <h5>Member Since: <span class="text-muted">{{(Auth::user()->created_at)->diffForHumans()}}</span></h5>
    </div>
</div>

{{-- Choose Avatar --}}
<div class="modal" id="avatarModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change Profile Picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
          <img src="{{asset('images/avatar0.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar1.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar2.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar3.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar4.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar5.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar6.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar7.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar8.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar9.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar10.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
          <img src="{{asset('images/avatar11.png')}}" alt="" class="avatar m-3"  height="80px" width="80px">
        </div>
      </div>
      <div class="modal-footer">
        <form action="/changeavatar" method="POST">
          @csrf
          @method('PATCH')
          <input type="hidden" name="avatarName" id="txtAvatar" >
          <button type="submit" class="btn btn-primary d-none" id="btnChangeAvatar">Save changes</button>
        </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
{{-- Modal
<div class="modal" id="editModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Update Profile</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <input type="hidden" class="form-control" name="currentPassword" placeholder="Current Password" value="{{Auth::user()->password}}">
          <div class="form-group">
              <label for="name">Name</label>
               <input type="text" class="form-control" name="name" placeholder="name" value="{{Auth::user()->name}}">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
             <input type="email" class="form-control" name="email" placeholder="Email" value="{{Auth::user()->email}}">
          </div>
          <div class="form-group">
            <label for="currentPassword">Current Password</label>
             <input type="password" class="form-control" name="currentPassword" placeholder="Current Password" value="">
          </div>
          <div class="form-group">
            <label for="newPassword">New Password</label>
             <input type="text" class="form-control" name="currentPassword" placeholder="Current Password" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div> --}}
@endsection
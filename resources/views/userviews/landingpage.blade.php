@extends('layouts.app')
@php ($badgeColor = array('badge-primary','badge-secondary','badge-success','badge-danger','badge-warning','badge-info','badge-dark'))
@section('side')
<div class="text-center">
    <div class="info card bg-light my-2">
        <img src="{{asset(Auth::user()->avatar)}}" height="75px" width="75px">
        @Auth
            <h4 class="p-3">hello <span class="text-info">{{strtoupper(trans(Auth::user()->name))}}</span></h4>
            <button class="mb-4 px-5 btn btn-success" data-toggle="modal" data-target="#createQuizModal">Create a Quiz</button>
        @endAuth
    </div>
    
    <div class="achievementsCard card mb-3" >
        <div class="card-body">
            <h4 class="card-title text-left text-warning">Achievements</h4>
            @if($numLike >=3)
            <img src="{{asset('images/trophy1colored.png')}}" height="50px" width="50px">
            @else
            <img src="{{asset('images/trophy1black.png')}}" data-toggle="tooltip" data-placement="bottom" title="Popular Award | Receive at least 3 stars to unlock it." height="50px" width="50px">
            @endif
            @if($numQuiz >=10)
            <img src="{{asset('images/trophy2colored.png')}}" height="50px" width="50px">
            @else
            <img src="{{asset('images/trophy2black.png')}}" data-toggle="tooltip" data-placement="bottom" title="Sensei Award | Post at least 10 quizzes to unlock it." height="50px" width="50px">
            @endif
            @if($countCorrectAnswer >=10)
            <img src="{{asset('images/trophy3colored.png')}}" height="50px" width="50px">
            @else
            <img src="{{asset('images/trophy3black.png')}}" data-toggle="tooltip" data-placement="bottom" title="Senpai Award | Have at least ten correct answers to unlock it." height="50px" width="50px">
            @endif
        </div>
    </div>
    <hr>
    <div class="categories">
        <p class="text-left lead text-dark">Filter by Category</p>
        @foreach ($categories as $category)
            <a href="/quiz/{{$category->id}}" class="badgeCategory2 badge badge-pill {{$badgeColor[array_rand($badgeColor)]}} m-1 text-white">{{$category->category}}</a>
        @endforeach
    </div>
</div>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12 px-5">
        <form action="/search"  method="POST">
            @csrf
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search quiz by question">
                <div class="input-group-append">
                    <button class="btn btn-info">Search</button>
                </div>
            </div>  
        </form>
        @foreach($quizzes as $quiz)
            @if($quiz->user_id !== Auth::user()->id && count($quiz->answer) == 0)
            {{-- {{$quiz->answer}} --}}
            {{-- @php($countLike = 0)
            @foreach($likes as $like)
                @if($quiz->id == $like->quiz_id)
                    @php($countLike++)
                @endif
            @endforeach
            {{$countLike}} --}}
                <div class="bg-light p-4 my-2 position-relative">
    {{-- Quiz Div --}}
                    <div id="quizForm{{$quiz->id}}" class="quizForm">
                        <div>
                           <span class="text-secondary">Posted by: {{ucfirst($quiz->user->name)}}</span> 
                           <i class="reportIcon fas fa-exclamation-circle text-danger my-2" title="Report!" onclick="showReportDiv({{$quiz->id}})"></i>
                           @php($liked = false)
                           @foreach($likes as $like)
                               @if($like->quiz_id == $quiz->id)
                               @php($liked = true)
                                <form class="likeIcon" action="/unlike/{{$like->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="likeIconBtn">
                                        <i class="text-secondary fas fa-star text-warning"></i>  
                                    </button>
                                    {{-- <i class="likeIcon text-secondary fas fa-star text-warning" onclick="unlikeQuiz({{$quiz->id}})"></i>  --}}
                                </form>
                               @endif
                           @endforeach
                           @if($liked == false)
                                <form class="likeIcon" action="/like/{{$quiz->id}}" method="POST">
                                    @csrf
                                    <button type="submit" class="likeIconBtn">
                                        <i class="text-secondary fas fa-star text-secondary"></i>  
                                    </button>
                                    {{-- <i class="likeIcon text-secondary fas fa-star text-warning" onclick="unlikeQuiz({{$quiz->id}})"></i>  --}}
                                </form>
                           @endif
                        </div>
                        <h5 class="my-3">{{$quiz->question}}</h5>
                        <div class="form-group">
                            @foreach($quiz->option as $key => $option)
                                <div class="custom-control custom-radio px-5">
                                <input type="radio" id="{{$option->id}}" value="{{$key}}" name="optionRadio{{$quiz->id}}" class="option{{$quiz->id}} custom-control-input" checked="">
                                    <label class="custom-control-label" for="{{$option->id}}">{{$option->option}}</label>
                                </div>
                            @endforeach
                                    <input type="radio" id="x" value="x" name="optionRadio{{$quiz->id}}" class="custom-control-input invisible" checked="">
                                    <label class="custom-control-label invisible" for="x">x</label>
                        </div>
                        <div class="text-right px-5">
                                <button type="submit" id="btnSubmit{{$quiz->id}}" class="btnSubmit btn btn-info" onclick="answerBtn({{$quiz->id}},{{$quiz->answerKey}})">Submit</button>
                                <h5 id="validateAnswer{{$quiz->id}}" class="validateAnswer text-right"></h5>
                        </div>
                        <p class="text-secondary text-left">{{$quiz->created_at->diffForHumans()}}</p>
                        {{-- <input type="hidden" id="answerKey{{$quiz->id}}" name="answerKey" value="{{$quiz->answerKey}}"> --}}
                    </div>
    {{-- Report Div --}}
                    <div class="reportDiv form-group text-dark bg-dark p-4 d-none position-absolute">
                        <div class="text-right">
                            <i class="btnCloseReport fas fa-times text-white" onclick="closeReportBtn({{$quiz->id}})"></i>
                        </div>
                        <h3 class="text-danger">Report</h3>
                        <p class="text-light">Question contains:</p>
                        {{-- @php ($badgeColor = array('badge-primary','badge-secondary','badge-success','badge-warning','badge-info')) --}}
                        <div class="text-center">
                            <form class="text-white" method="POST" action="/report">
                                <input type="hidden" name="quizId" value="{{$quiz->id}}">

                                @csrf
                                @foreach($violations as $violation)
                                    <label class="checkbox-inline mr-4">
                                        <input type="checkbox" name="{{$violation->id}}" value="{{$violation->id}}">{{$violation->violation}}
                                    </label>
                                @endforeach
                                <div class="text-center">
                                    <button class="btn btn-danger mt-5 px-5">Report</button>
                                </div>
                            </form>
                        </div>
                       
                    </div>
                </div>
            @endif
		@endforeach
	</div>
	
</div>


   {{-- Modals --}}
   <div class="modal" id="createQuizModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create a Quiz</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- <form action="/createquiz" method="POST" class="mb-3">
                    @csrf --}}
                    @foreach ($categories as $category)
                        <a class="badgeCategory badge badge-pill {{$badgeColor[array_rand($badgeColor)]}} m-1 text-white">{{$category->category}}</a>
                    @endforeach
               
                    <div class="form-group">
                        <textarea class="form-control" id="question" name="question" rows="3" placeholder="Question" required></textarea>
                      </div>
                    <div class="input-group mb-3">
                        <span class="btnCheck input-group-prepend input-group-text"><i class="fas fa-check"></i></span>
                            <input type="text" id="option1" name="option1" class="form-control" placeholder="Option1" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="btnCheck input-group-prepend input-group-text"><i class="fas fa-check"></i></span>
                            <input type="text" id="option2" name="option2" class="form-control" placeholder="Option 2" required>
                    </div>
                    <div class="input-group alert alert-dismissible mb-3 w-100 p-0">
                        <span class="btnCheck input-group-prepend input-group-text"><i class="fas fa-check"></i></span>
                            <input type="text" id="option3" name="option3" class="form-control" placeholder="Option 3" required>
                        {{-- <span class="btnRemove input-group-append input-group-text"><i class="fas fa-trash-alt"></i></span> --}}
                    </div>
                    <div class="input-group alert alert-dismissible mb-3 w-100 p-0">
                        <span class="btnCheck input-group-prepend input-group-text"><i class="fas fa-check"></i></span>
                            <input type="text" id="option4" name="option4" class="form-control" placeholder="Option 4" required>
                        {{-- <span class="btnRemove input-group-append input-group-text"><i class="fas fa-trash-alt"></i></span> --}}
                    </div>
                    <input type="hidden" id="keyAnswer" name="keyAnswer" required>
                    <input type="hidden" id="categoryId" name="categoryId" required>
                    <button  type="submit" class="btn btn-success" onclick="postQuiz()">Post it</button>
                {{-- </form> --}}
            </div>
        </div>
    </div>
</div>
@endsection
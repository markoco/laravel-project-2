@extends('layouts.admin')
@section('content')
    @php ($badgeColor = array('badge-primary','badge-secondary','badge-success','badge-danger','badge-warning','badge-info','badge-dark'))
   
    <div class="col-lg-6 offset-lg-3 bg-light p-5 card">
        <h4 class="text-left mb-1">Update Quiz</h4>
        <hr>
        <div>
            @foreach ($categories as $category)
                <a class="badgeCategory badge badge-pill {{$badgeColor[array_rand($badgeColor)]}} m-1 text-white">{{$category->category}}</a>
            @endforeach
        </div>
        <form action="/updatequiz/{{$quiz->id}}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-group">
            <textarea class="form-control" id="question" name="question" rows="3" placeholder="Question" required>{{$quiz->question}}</textarea>
            </div>
            @foreach ($quiz->option as $key => $option)
                @if($quiz->answerKey == $key+1)
                    <div class="input-group mb-3">
                        <span class="btnCheck input-group-prepend input-group-text text-success"><i class="fas fa-check"></i></span>
                        <input type="text" id="option1" name="option{{$key+1}}" class="form-control is-valid" value="{{$option->option}}" placeholder="Option1" required>
                        <input type="hidden"  name="{{$key+1}}" class="form-control" value="{{$option->id}}">
                    </div>  
                @else
                    <div class="input-group mb-3">
                        <span class="btnCheck input-group-prepend input-group-text"><i class="fas fa-check"></i></span>
                        <input type="text" id="option1" name="option{{$key+1}}" class="form-control" value="{{$option->option}}" placeholder="Option1" required>
                        <input type="hidden"  name="{{$key+1}}" class="form-control" value="{{$option->id}}">
                    </div> 
                @endif 
            @endforeach
            
        
            <input type="hidden" id="keyAnswer" name="keyAnswer" value="{{$key+1}}" required>
                <input type="hidden" id="categoryId" name="categoryId" value={{$quiz->category_id}} required>
            <button  type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
   
@endsection
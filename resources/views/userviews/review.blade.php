@extends('layouts.app')
@php ($badgeColor = array('badge-primary','badge-secondary','badge-success','badge-danger','badge-warning','badge-info','badge-dark'))
@section('side')
<div class="text-center">
    <div class="d-flex flex-column">
        <div class="reviewScore card text-white bg-info mb-3">
            <div class="card-body">
                <h4 class="card-title text-left">Overall Score</h4>
                <h3>{{round($score)}}%</h3>
            </div>
        </div>
        <div class="reviewStat card text-white bg-info mb-3">
            <div class="card-body">
                <h4 class="card-title text-left">Statistics</h4>
                <p class="text-white text-left">Quiz Taken: {{count($allAnswer)}}</p>
                <p class="text-white text-left">Correct Answers: {{$countCorrectAnswer}}</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="categories">
        <p class="text-left lead text-dark">Filter by Category</p>
        @foreach ($categories as $category)
            <a href="/quiz/{{$category->id}}" class="badgeCategory2 badge badge-pill {{$badgeColor[array_rand($badgeColor)]}} m-1 text-white">{{$category->category}}</a>
        @endforeach
    </div>
</div>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 px-5">
            <form action="/search"  method="POST">
                @csrf
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search quiz by question">
                    <div class="input-group-append">
                        <button class="btn btn-info">Search</button>
                    </div>
                </div>  
            </form>
            @foreach($allAnswer as $answer)
            <div class="bg-light p-4 my-2 position-relative">
        {{-- Quiz Div --}}
                <div id="quizForm{{$answer->quiz_id}}" class="quizForm position-relative">
                    <div>
                        <span class="text-secondary">Posted by: {{ucfirst($answer->quiz->user->name)}}</span> 
                        <i class="reportIcon fas fa-exclamation-circle text-danger my-2" title="Report!" onclick="showReportDiv({{$answer->quiz->id}})"></i>                      
                                @php($liked = false)
                                @foreach($likes as $like)
                                    @if($like->quiz_id == $answer->quiz->id)
                                    @php($liked = true)
                                        <form class="likeIcon" action="/unlike/{{$like->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="likeIconBtn">
                                                <i class="text-secondary fas fa-star text-warning"></i>  
                                            </button>
                                            {{-- <i class="likeIcon text-secondary fas fa-star text-warning" onclick="unlikeQuiz({{$quiz->id}})"></i>  --}}
                                        </form>
                                    @endif
                                @endforeach
                                @if($liked == false)
                                        <form class="likeIcon" action="/like/{{$answer->quiz->id}}" method="POST">
                                            @csrf
                                            <button type="submit" class="likeIconBtn">
                                                <i class="text-secondary fas fa-star text-secondary"></i>  
                                            </button>
                                            {{-- <i class="likeIcon text-secondary fas fa-star text-warning" onclick="unlikeQuiz({{$quiz->id}})"></i>  --}}
                                        </form>
                                @endif
                    </div>
                    <h5 class="my-3">{{$answer->quiz->question}}</h5>
                    <div class="form-group">
                        <ul>
                            @foreach($answer->quiz->option as $key => $option)
                                @if($answer->answer == $key+1)
                                    @if($answer->answer == $answer->quiz->answerKey)
                                        <b><li id="listKeyAns" class="text-success">{{$option->option}}</li></b>
                                    @else
                                        <b><li class="text-danger">{{$option->option}}</li></b>
                                    @endif
                                @else
                                    <li>{{$option->option}}</li>
                                @endif
                                @if($answer->quiz->answerKey == $key+1)
                                    <h6 class="keyAns text-right">Answer Key: <span class="text-success">{{$option->option}}</span></h6>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <h4 id="h4KeyAns" class="text-success text-right"> </h4>
                    <p class="text-secondary text-left">{{$answer->created_at->diffForHumans()}}</p>
                </div>
        {{-- Report Div --}}
                <div class="reportDiv form-group text-dark bg-dark p-4 d-none position-absolute">
                    <div class="text-right">
                        <i class="btnCloseReport fas fa-times text-white" onclick="closeReportBtn({{$answer->quiz->id}})"></i>
                    </div>
                    <h3 class="text-danger">Report</h3>
                    <p class="text-light">Question contains:</p>
                    {{-- @php ($badgeColor = array('badge-primary','badge-secondary','badge-success','badge-warning','badge-info')) --}}
                    <div class="text-center">
                        <form class="text-white" method="POST" action="/report">
                            <input type="hidden" name="quizId" value="{{$answer->quiz->id}}">

                            @csrf
                            @foreach($violations as $violation)
                                <label class="checkbox-inline mr-4">
                                    <input type="checkbox" name="{{$violation->id}}" value="{{$violation->id}}">{{$violation->violation}}
                                </label>
                            @endforeach
                            <div class="text-center">
                                <button class="btn btn-danger mt-5 px-5">Report</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-6">
        <h1>Categories</h1>
        <div class="card bg-light mb-3" style="max-width: 30rem;">
            <div class="card-body">
            <h4 class="card-title">Categories</h4>
            
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <h1>Violations</h1>
        <div class="card bg-light mb-3" style="max-width: 30rem;">
            <div class="card-body">
            <h4 class="card-title">Violations</h4>
            <table class="mx-auto">
            
                <tbody>
                    @foreach ($violations as $violation)
                        <tr>
                            <td>{{$violation->violation}}</td>
                            <td>
                                <button class="btn btn-warning ml-5">Delete</button>
                                <button class="btn btn-success ">Update</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
   
@endsection
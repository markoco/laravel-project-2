@extends('layouts.admin')
@section('content')
<h1 class="text-center p-2">Admin's Dashboard</h1>
{{-- <button class="btn btn-success" data-toggle="modal" data-target="#violationModal">Violations</button>
<button class="btn btn-warning" data-toggle="modal" data-target="#categoryModal">Categories</button> --}}
<hr>
<div class="row d-flex">
	<div class="card text-white bg-info m-3" style="min-width: 20rem;">
	  <div class="card-body">
	    <h4 class="card-title">Reviewers</h4>
	    <h1>{{$users}} <span class="lead text-white"> Reviewers</span></h1>
	  </div>
	</div>

	<div class="card text-white bg-info m-3" style="min-width: 20rem;">
	  <div class="card-body">
	    <h4 class="card-title">Quizzes</h4>
	    <h1>{{$numQuiz}}<span class="lead text-white"> Quizzes</span></h1>
	  </div>
	</div>

	<div class="card text-white bg-info m-3" style="min-width: 20rem;">
	  <div class="card-body">
	    <h4 class="card-title">Likes</h4>
	    <h1>{{$likes}} <span class="lead text-white"> Likes</span></h1>
	  </div>
	</div>

	<div class="card text-white bg-info m-3" style="min-width: 20rem;">
	  <div class="card-body">
	    <h4 class="card-title">Statistics</h4>
	  <h1>{{round($score)}}%<span class="lead text-white"> Average Score</span></h1>
	  </div>
	</div>

	{{-- <div class="card text-white bg-info m-3" style="min-width: 20rem;">
	  <div class="card-body">
	    <h4 class="card-title">Reports</h4>
	    <h1>{{count($reports)}} 
	    	<a href="/admin" class="btn btn-danger" type="submit">Reported quiz</a>
		</h1>
	  </div>
	</div> --}}
{{-- 
	<div class="card text-dark bg-light m-3" style="min-width: 20rem;">
		<div class="card-body text-center">
		  <button class="btn btn-success" data-toggle="modal" data-target="#violationModal">Violations</button>
		  <button class="btn btn-warning" data-toggle="modal" data-target="#categoryModal">Categories</button>
		</div>
	</div> --}}


	<table id="test">

	</table>

</div>


<table class="table table-striped bordered">
    <thead>
        <tr>
            <th>Report Id</th>
            <th>Quiz Id</th>
            <th>Violation</th>
            <th>Quiz Owner</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reports as $report)
            <tr id="row{{$report->id}}">
                <td>{{$report->id}}</td>
                <td>{{$report->quiz_id}}</td>
                <td>
                    @foreach($report->violations as $violation)
                      {{$violation->violation}}
                    @endforeach
                </td>
                <td>{{$report->quiz->user->name}}</td>
                <td>
                    {{$report->created_at}}
                </td>
                <td class="d-flex">
                    @if($report->quiz->user->status == 'Active')
                    <button class="btn btn-success mx-2" onclick="banUser({{$report->id}},{{$report->quiz->user->id}},{{$report->quiz_id}})">Ban User</button>
                    @else
                    <button class="btn btn-info mx-2" onclick="banUser({{$report->id}},{{$report->quiz->user->id}},{{$report->quiz_id}})">Unbanned</button>
                    @endif

                    <form action="/deletequiz/{{$report->quiz->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-warning mx-2">Delete Quiz</button>
                    </form>

                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="row">
  <div class="col-lg-12 d-flex justify-content-center">
    {{$reports->links()}}
  </div>
</div>

{{-- Modal Category --}}
<div class="modal" id="categoryModal">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title">Categories</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body" style="overflow-y:auto">
			<div id="categoryBody" class="">
				<div class="form-group">
					<label class="control-label">Category Name:</label>
					<div class="form-group">
					  <div class="input-group mb-3">
						<input type="text" name="newCategory" id="newCategory" class="form-control">
						<div class="input-group-append">
						  <button class="btn btn-info" onclick="addNewCategory()">Add</button>
						</div>
					  </div>
					</div>
				  </div>
				<table class="mx-auto">
					<tbody>
						@foreach ($categories as $category)
						<tr id="tr{{$category->id}}">
								<td>{{$category->category}}</td>
								<td>
								{{-- <form action="/deletecategory/{{$category->id}}" method="POST">
									@csrf
									@method('DELETE') --}}
									<button class="btn btn-warning ml-5" onclick="deleteCategory({{$category->id}})">Delete</button>
									<button class="btn btn-success" onclick="editCategory({{$category->id}},'{{$category->category}}')">Edit</button>
								{{-- </form> --}}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			</div>
			<div id="categoryUpdate" class="d-none">
				<div class="form-group mx-5">
					<label for="categoryname">Category Name</label>
					<input type="hidden" class="form-control" name="categoryId"  id="categoryId">
					<input type="text" class="form-control" name="categoryName"  id="categoryName" placeholder="Category Name" autocomplete="off">
					<div class="text-center p-3">
						<button class="btn btn-info" onclick="updateCategory()">Update</button>
					</div>
				</div>
			</div>
		<div class="modal-footer">
		  <button type="button" id="backToCategory" class="btn btn-success d-none" onclick="backToCategory()">Back</button>
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
</div>

{{-- Modal Violation --}}
<div class="modal" id="violationModal">
	<div class="modal-dialog" role="document">
	  <div class="modal-content">
		<div class="modal-header">
		  <h5 class="modal-title">Violations</h5>
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		  </button>
		</div>
		<div class="modal-body">
			<div id="violationBody" class="">
				<div class="form-group">
					<label class="control-label">Violation Name:</label>
					<div class="form-group">
					  <div class="input-group mb-3">
						<input type="text" name="newViolation" id="newViolation" class="form-control">
						<div class="input-group-append">
						  <button class="btn btn-info" onclick="addNewViolation()">Add</button>
						</div>
					  </div>
					</div>
				  </div>
				<table class="mx-auto">
					<tbody>
						@foreach ($violations as $violation)
							<tr id="trViolation{{$violation->id}}">
								<td>{{$violation->violation}}</td>
								<td>
									<button class="btn btn-warning ml-5" onclick="deleteViolation({{$violation->id}})">Delete</button>
									<button class="btn btn-success "  onclick="editViolation({{$violation->id}})">Edit</button>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			</div>
			<div id="violationUpdate" class="d-none">
				<div class="form-group mx-5">
					<label for="violationName">Violation Name</label>
					<input type="hidden" class="form-control" name="violationId"  id="violationId">
					<input type="text" class="form-control" name="violationName"  id="violationName" placeholder="Category Name" autocomplete="off">
					<div class="text-center p-3">
						<button class="btn btn-info" onclick="updateViolation()">Update</button>
					</div>
				</div>
			</div>
			
		<div class="modal-footer">
			<button type="button" id="backToViolation" class="btn btn-success d-none" onclick="backToViolation()">Back</button>
		  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
</div>
@endsection
@extends('layouts.admin')
@section('content')
<table class="table table-striped bordered">
    <thead>
        <tr>
            <th>Report Id</th>
            <th>Quiz Id</th>
            <th>Violation</th>
            <th>Quiz Owner</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reports as $report)
            <tr id="row{{$report->id}}">
                <td>{{$report->id}}</td>
                <td>{{$report->quiz_id}}</td>
                <td>
                    @foreach($report->violations as $violation)
                      {{$violation->violation}}
                    @endforeach
                </td>
                <td>{{$report->quiz->user->name}}</td>
                <td>
                    {{$report->created_at}}
                </td>
                <td>
                    @if($report->quiz->user->status == 'Active')
                    <button class="btn btn-success m-2" onclick="banUser({{$report->id}},{{$report->quiz->user->id}},{{$report->quiz_id}})">Ban User</button>
                    @else
                    <button class="btn btn-info m-2" onclick="banUser({{$report->id}},{{$report->quiz->user->id}},{{$report->quiz_id}})">Unbanned</button>
                    @endif

                    <form action="/deletequiz/{{$report->quiz->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-warning m-2">Delete Quiz</button>
                    </form>

                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="row">
  <div class="col-lg-12 d-flex justify-content-center">
    {{$reports->links()}}
  </div>
</div>

<div class="modal" id="reviewModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="adminQuestion"> </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
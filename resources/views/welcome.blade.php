<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     {{-- Fonstawesome --}}
    <script src="https://kit.fontawesome.com/b4cdddd749.js" crossorigin="anonymous"></script>
    <style>
        #landingImage{
            transition: 1s;
            margin-left: -200px;
            opacity: 0;
        }
        #landingBtn{
            opacity: 0;
            transition: 2s;
        }
        #landingText{
            opacity: 0;
            transition: 4s;
        }
        #landingP{
            opacity: 0;
            transition: 3s;
        }
    </style>
    <title>Document</title>
</head>
<body class="bg-info">
    <div class="container vh-100 d-flex flex-column justify-content-center align-items-center">
        <h1 id="landingText" class="text-light">CSC Quiz Game</h1>
        <p id="landingP" class="lead text-white">When you have fun, then you're more interested in learning</p>
        <img id="landingImage" src="{{ asset('images/study.png') }}" width="500px" height="300px">
        @if (!Auth::guest())
        <a href="/home" id="landingBtn" class="btn btn-light pl-5" >Get Started <i class="ml-3 fas fa-long-arrow-alt-right text-dark"></i></a>
        @else
            <button id="landingBtn" class="btn btn-light pl-5"  data-toggle="modal" data-target="#loginModal">Get Started <i class="ml-3 fas fa-long-arrow-alt-right text-dark"></i></button>
        @endif
</div>

<div class="modal" id="loginModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <div>
            <form method="POST" action="{{ route('login') }}" class="p-4">
                @csrf
                <label for="email">{{ __('E-Mail Address') }}</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-envelope"></i></span>
                    </div>
                    <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror 
                </div>
                <label for="password">{{ __('Password') }}</label>
                <div class="input-group mb-5">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                {{-- <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div> --}}
                <button type="submit" class="btn btn-info px-5">
                    {{ __('Login') }} 
                </button>
                

                <div class="form-group row mb-0">
                    <div>
                        @if (Route::has('password.request'))
                            <a class="btn btn-link text-info" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                        @if (Route::has('register'))
                            <a class="btn btn-link text-info" href="{{ route('register') }}">Register</a>
                        @endif
                    </div>
                </div>
            
        </div>
        </div>
        <div class="modal-footer">
           
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  
{{-- Bootstrap JQuery --}}
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

<script>
window.onload = function() {
  let img = document.getElementById('landingImage')
  img.style.marginLeft = "25px";
  img.style.opacity = "1";

  let btn = document.getElementById('landingBtn');
  btn.style.opacity = "1";

  let text = document.getElementById('landingText')
  text.style.opacity = "1";

  let p = document.getElementById('landingP')
  p.style.opacity = "1";
};
</script>
</html>
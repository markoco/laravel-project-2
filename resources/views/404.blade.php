<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
      body{
          font-family: 'Ropa Sans', sans-serif;
          margin-top: 100px;
          background-color: #F0CA00;
          background-color: #F3661C;
          text-align: center;
          color: #fff;
          
          background: url('https://cdn.pixabay.com/photo/2017/01/26/21/32/background-2011768_960_720.jpg') no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
      }
      .error-heading{
          margin: 50px auto;
          width: 250px;
          border: 5px solid #fff;
          font-size: 126px;
          line-height: 126px;
          border-radius: 30px;
          text-shadow: 6px 6px 5px #000;
      }
      .error-heading img{
          width: 100%;
      }
      .error-main h1{
          font-size: 75px;
          margin: 0px;
          text-shadow: 6px 6px 5px #000;
          color: white;
      }
     
    </style>
</head>
<body>

	<div class="error-main">
		<h1>Oops!</h1>
		<div class="error-heading">403</div>
		<a class="btn btn-light mb-5" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
		<p>You do not have permission to access the program that you requested.</p>
	</div>
</body>
</html>